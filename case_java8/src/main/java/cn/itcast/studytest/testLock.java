package cn.itcast.studytest;

import java.util.concurrent.locks.ReentrantLock;

public class testLock {
    private static ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) {
//        try {
//            System.out.println("enter main");
//            method1();
//        }finally {
//            lock.unlock();
//        }

        Thread t1 = new Thread(() -> {
            System.out.println("可打断锁");
            try {
                lock.lockInterruptibly();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            try {
                System.out.println("获取到锁");
            }finally {
                lock.unlock();
            }
        }, "t1");

        lock.lock();
        t1.start();
        t1.interrupt();

    }

    private static void method1() {
        lock.lock();
        System.out.println("enter method1");
        method2();
    }

    private static void method2() {
        lock.lock();
        System.out.println("enter method2");
    }


}
