package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "c.test23")
public class test23 {
    private static ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.debug("尝试获取锁");
            try {
                if (!lock.tryLock(2, TimeUnit.SECONDS)){
                    log.debug("没有获取到锁");
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("已获取到锁");
        }, "t1");
        lock.lock();
        t1.start();
        Sleeper.sleep(1);
        log.debug("已经释放锁");
        lock.unlock();
    }
}
