package cn.itcast.studytest;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestStr {
    public static void main(String[] args) {
         Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        String str=" this is test text ! ";
        Matcher m = p.matcher(str);
        String outStr = m.replaceAll("");
        System.out.println(outStr);
        String s = " this is test text ! ";
        System.out.println(s.replaceAll("\\s*", ""));
    }
}
