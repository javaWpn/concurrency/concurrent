package cn.itcast.studytest;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class test31 {
    public static void main(String[] args) {
        AwaitSignal awaitSignal = new AwaitSignal(5);
        Condition awaitSet = awaitSignal.newCondition();
        Condition bwaitSet = awaitSignal.newCondition();
        Condition cwaitSet = awaitSignal.newCondition();
        new Thread(()->{
            awaitSignal.print("a",awaitSet,bwaitSet);
        },"t1").start();
        new Thread(()->{
            awaitSignal.print("b",bwaitSet,cwaitSet);
        },"t2").start();
        new Thread(()->{
            awaitSignal.print("c",cwaitSet,awaitSet);
        },"t3").start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        awaitSignal.lock();
        try {
            awaitSet.signal();
        }finally {
            awaitSignal.unlock();
        }
    }
}

class AwaitSignal extends ReentrantLock {
    private int loopNUmber;

    public AwaitSignal(int loopNUmber) {
        this.loopNUmber = loopNUmber;
    }

    public void print(String str, Condition current, Condition next){
        for (int i = 0; i < loopNUmber; i++) {
            lock();
            try {
                try {
                    current.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(str);
                next.signal();
            }finally {
                unlock();
            }
        }
    }
}