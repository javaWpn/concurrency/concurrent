package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest10")
public class Test10 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()){

            }
            log.debug("被打断了");
        }, "t1");
        t1.start();
        Thread.sleep(1000);
        t1.interrupt();
    }
}
