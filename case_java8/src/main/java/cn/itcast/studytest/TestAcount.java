package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
@Slf4j(topic = "c.TestAcount")
public class TestAcount {

    public static void main(String[] args) throws InterruptedException {
        Acount a = new Acount(1000);
        Acount b = new Acount(1000);

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                a.transfer(b,randomInt());
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                b.transfer(a,randomInt());
            }
        }, "t2");

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        //检查a和b最后的总金额是不是20000
        log.debug("总金额:{}",(a.getAcount() + b.getAcount()));

    }
    public static int randomInt(){
        Random random = new Random();
        return random.nextInt(5) + 1;
    }
}
class Acount{

    private int acount;

    public Acount(int acount) {
        this.acount = acount;
    }

    public int getAcount() {
        return acount;
    }

    public void setAcount(int acount) {
        this.acount = acount;
    }

    public void transfer(Acount target,int amount){
        synchronized (Acount.class){
            if (this.acount >= amount){
                this.setAcount(this.getAcount() - amount);
                target.setAcount(target.getAcount() + amount);
            }
        }
    }

}
