package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest11")
public class Test11 {
    public static void main(String[] args) throws InterruptedException {
        MiniterThread miniterThread = new MiniterThread();
        miniterThread.start();
        Thread.sleep(3500);
        miniterThread.stop();
}
}
//俩阶段终止模式
@Slf4j(topic = "c.MiniterThread")
class MiniterThread{

    private Thread miniterThread;

    void start(){
        miniterThread = new Thread(() -> {
            Thread currentThread = Thread.currentThread();
            while (true){
                if (currentThread.isInterrupted()){
                    log.debug("料理后事");
                    break;
                }
                try {
                    Thread.sleep(1000);
                    log.debug("执行监控记录");
                } catch (InterruptedException e )//当在休眠中打断后进入此异常会重新重置currentThread.isInterrupted()为假
                {
                    e.printStackTrace();
                    stop();//所以此处还需要打断一次 不然依然无法退出死循环
                }
            }
        }, "t1");
        miniterThread.start();
    }

    void stop(){
        miniterThread.interrupt();
    }
}
