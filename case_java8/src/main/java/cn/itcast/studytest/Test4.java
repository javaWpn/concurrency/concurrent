package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest4")
public class Test4 {
    public static void main(String[] args) {
        Thread t1 = new Thread("t1") {
            @Override
            public void run() {
                log.info("running.........");
            }
        };
        t1.run();
        log.info(t1.getState().toString());
        log.info("main running.......");
        t1.start();
        log.info(t1.getState().toString());
    }
}
