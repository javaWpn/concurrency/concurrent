package cn.itcast.studytest;

import cn.itcast.pattern.Downloader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

@Slf4j(topic = "c.test19")
public class Test19 {
    public static void main(String[] args) {
        //需求是t1等待t2的结果才执行
        Guarded guarded = new Guarded();

        new Thread(()->{
            log.debug("执行等待结果。。。。。。");
            List<String> result = (List<String>)guarded.get(2000);
            log.debug("结果已等到。。。。。。");
            log.debug("结果大小{}",result.size());
        },"t1").start();

        new Thread(()->{
            log.debug("执行下载结果。。。。。。");
            try {
                List<String> download = Downloader.download();
                log.debug("下载完成。。。。。。");
                //去通知结果已经下载完成了
                guarded.complete(download);
            } catch (IOException e) {
                e.printStackTrace();
            }

        },"t2").start();
    }
}


class Guarded{
    //声明一个结果对象
    private Object response;

    //声明一个未获取结果的方法
    public Object get(long timeout){
        synchronized (this){
            long begin = System.currentTimeMillis();
            long passedTime = 0;
            long waitTime = timeout - passedTime;
            while (null == response){
                if (waitTime <= 0){
                    break;
                }
                try {
                    this.wait(waitTime);//防止被虚假唤醒，此处不应该是timeout 而是timeout - passedTime
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passedTime = System.currentTimeMillis() - begin;
            }
            return response;
        }
    }

    //声明一个获取到结果的方法
    public void complete(Object response){
       synchronized (this){
           this.response = response;
           this.notifyAll();
       }
    }
}
