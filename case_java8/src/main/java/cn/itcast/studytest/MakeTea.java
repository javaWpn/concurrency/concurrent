package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.MakeTea")
//洗茶壶，洗茶杯，洗水壶，烧开水，泡茶，拿茶叶
public class MakeTea {
    public static void main(String[] args) {
        //t1,洗完水壶，才能烧开水，所以这个是串行进行的
        Thread t1 = new Thread(() -> {
            Sleeper.sleep(1);
            log.debug("洗水壶");
            Sleeper.sleep(5);
            log.debug("烧开水");
        }, "t1");
        //t2,洗茶壶，洗茶杯，拿茶叶
        Thread t2 = new Thread(() -> {
            Sleeper.sleep(1);
            log.debug("洗茶壶");
            Sleeper.sleep(4);
            log.debug("洗茶杯");
            Sleeper.sleep(1);
            log.debug("拿茶叶");
            try {
                t1.join();
                log.debug("泡茶");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t2");
        t1.start();
        t2.start();
    }
}
