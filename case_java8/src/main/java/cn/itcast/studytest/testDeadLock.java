package cn.itcast.studytest;

import static cn.itcast.n2.util.Sleeper.sleep;

public class testDeadLock {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        Object a = new Object();
        Object b = new Object();

        Thread t1 = new Thread(() -> {
            synchronized (a){
                System.out.println(Thread.currentThread().getName()+": a");
                sleep(1);
                synchronized (b){
                    System.out.println(Thread.currentThread().getName()+": b");
                }
            }
        }, "t1");

        Thread t2 = new Thread(() -> {
            synchronized (b){
                System.out.println(Thread.currentThread().getName()+": b");
                sleep(0.5);
                synchronized (a){
                    System.out.println(Thread.currentThread().getName()+": a");
                }
            }
        }, "t2");
        t1.start();
        t2.start();
    }
}
