package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import static java.lang.Thread.sleep;

@Slf4j(topic = "c.test16")
public class test16 {
    static final Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            log.debug("进入执行。。。。。。");
            synchronized (lock){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            log.debug("执行t1其他代码。。。。。。。");
        },"t1").start();
        new Thread(()->{
            log.debug("进入执行。。。。。。");
            synchronized (lock){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            log.debug("执行t2其他代码。。。。。。。");
        },"t2").start();
        Thread.sleep(2000);
        log.debug("唤醒其他线程。。。。。");
        //先获取锁对象
        synchronized (lock){
//            lock.notify();//唤醒其中一个线程
            lock.notifyAll();//唤醒所有线程
        }
    }
}
