package cn.itcast.studytest;

import java.util.LinkedList;

public class testSupportConsumer {
    public static void main(String[] args) {
        MessageQueues messageQueue = new MessageQueues(5);
        for (int i = 0; i < 10; i++) {
            int index = i;
            new Thread(()->{
                messageQueue.putMessage(new MessageBean(index,""+index));
            },"t"+index).start();
        }
        while (true){
            System.out.println(messageQueue.take());
        }
    }
}

final class MessageQueues{
    private LinkedList<MessageBean> list = new LinkedList<>();
    private int maxNum;

    public MessageQueues(int maxNum) {
        this.maxNum = maxNum;
    }

    public MessageBean take(){
        synchronized (list){
            while (list.isEmpty()){
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            list.notifyAll();
            return list.removeFirst();
        }
    }

    public void putMessage(MessageBean messageBean){
        synchronized (list){
            while (list.size() > maxNum){
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            list.add(messageBean);
            list.notifyAll();
        }
    }

}

class MessageBean{

    private int id;
    private String Content;

    public MessageBean(int id, String content) {
        this.id = id;
        Content = content;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return Content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", Content='" + Content + '\'' +
                '}';
    }
}