package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.propertyeditors.ReaderEditor;

import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "c.testPhilosopher")
public class testPhilosopher {
    public static void main(String[] args) {
//        Chopsticks c1 = new Chopsticks("c1");
//        Chopsticks c2 = new Chopsticks("c2");
//        Chopsticks c3 = new Chopsticks("c3");
//        Chopsticks c4 = new Chopsticks("c4");
//        Chopsticks c5 = new Chopsticks("c5");
//        new Philosopher("t1",c1,c2).start();
//        new Philosopher("t2",c2,c3).start();
//        new Philosopher("t3",c3,c4).start();
//        new Philosopher("t4",c4,c5).start();
//        new Philosopher("t5",c5,c1).start();//按c1，c5顺序会产生锁饥饿问题




        ChopsticksResolve cc1 = new ChopsticksResolve("c1");
        ChopsticksResolve cc2 = new ChopsticksResolve("c2");
        ChopsticksResolve cc3 = new ChopsticksResolve("c3");
        ChopsticksResolve cc4 = new ChopsticksResolve("c4");
        ChopsticksResolve cc5 = new ChopsticksResolve("c5");
        new PhilosopherResolve("t1",cc1,cc2).start();
        new PhilosopherResolve("t2",cc2,cc3).start();
        new PhilosopherResolve("t3",cc3,cc4).start();
        new PhilosopherResolve("t4",cc4,cc5).start();
        new PhilosopherResolve("t5",cc5,cc1).start();
    }
}
@Slf4j(topic = "c.Philosopher")
//哲学家就餐死锁问题
class Philosopher extends Thread{
    private String name;
    private Chopsticks left;
    private Chopsticks right;

    public Philosopher(String name,Chopsticks left, Chopsticks right) {
        super(name);
        this.name = name;
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true){
            synchronized (left){
                synchronized (right){
                    eat();
                }
            }
        }
    }

    private void eat() {
        log.debug("去吃饭");
    }
}
@Slf4j(topic = "c.PhilosopherResolve")
//解决哲学家就餐死锁问题
class PhilosopherResolve extends Thread{
    private ChopsticksResolve left;
    private ChopsticksResolve right;

    public PhilosopherResolve(String name,ChopsticksResolve left, ChopsticksResolve right) {
        super(name);
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        while (true){
            //尝试获得左筷子
            if (left.tryLock()){
                try {
                    //尝试获得右筷子
                    if (right.tryLock()){
                        try{
                            eat();
                        }finally {
                            right.unlock();
                        }
                    }
//                    log.debug("没有拿到右筷子");
                }finally {
                    left.unlock();
                }
            }
//            log.debug("没有拿到左筷子");
        }
    }

    private void eat() {
        log.debug("吃饭");
        Sleeper.sleep(0.5);
    }
}

class Chopsticks{
    private String name;

    public Chopsticks(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Chopsticks{" +
                "name='" + name + '\'' +
                '}';
    }
}

class ChopsticksResolve extends ReentrantLock{
    private String name;

    public ChopsticksResolve(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Chopsticks{" +
                "name='" + name + '\'' +
                '}';
    }
}