package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest8")
public class Test8 {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            int count = 0;
            for (;;){
                System.out.println("------>t1 "+count++);
            }
        }, "t1");
        Thread t2 = new Thread(() -> {
//            Thread.yield();
            int count = 0;
            for (;;){
                System.out.println("                   ------>t2 "+count++);
            }
        }, "t2");
        t1.start();
        t2.start();
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
    }
}
