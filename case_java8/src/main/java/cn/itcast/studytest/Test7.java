package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j(topic = "c.studyTest7")
public class Test7 {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.debug("t1 status:{}",Thread.currentThread().getState());
            try {
                log.debug("enter sleep....");
                TimeUnit.SECONDS.sleep(1);//睡眠1s TimeUnit更易看清楚睡眠多久
                log.debug("sleep end....");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t1");
        t1.start();
    }
}
