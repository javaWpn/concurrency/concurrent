package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

@Slf4j(topic = "c.TestThreadSafe")
public class TestThreadSafe {
    static final int THREAD_NUMBER = 2;
    static final int LOOP_NUMBER = 200;
    public static void main(String[] args) {
        ThreadSubSafe threadUnSafe = new ThreadSubSafe();
            for (int i = 0; i < LOOP_NUMBER; i++) {
                new Thread(() -> {
                    threadUnSafe.method1(LOOP_NUMBER);
                }, "Thread" + (i+1)).start();
            }
        }
    }
    class  ThreadUnSafe{
        ArrayList<String> list = new ArrayList();//成员变量是线程不安全的
        public  void  method1(int threadNumber){
            for (int i = 0; i < threadNumber; i++) {
                method2();
                method3();
        }
    }

    private void method2() {
        list.add("1");
    }

    private void method3() {
        list.remove(0);
    }
}
    class  ThreadSafe{
    public  void  method1(int threadNumber){
        ArrayList<String> list = new ArrayList();//不暴露引用的前提下局部变量是线程安全的
        for (int i = 0; i < threadNumber; i++) {
            method2(list);
            method3(list);
        }
    }

    private void method2(ArrayList<String> list) {
        list.add("1");
    }
    //切换为public后子类再重写后局部变量又变成了线程不安全的，private保证了子类不被重写覆盖调用，或者使用public final
    public void method3(ArrayList<String> list) {
        list.remove(0);
    }
}
    class  ThreadSubSafe extends  ThreadSafe{
        public void method3(ArrayList<String> list) {
            new Thread(()->{
                list.remove(0);
            },"t1").start();
        }
    }