package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static cn.itcast.n2.util.Sleeper.sleep;

@Slf4j(topic = "c.test25")
public class test25 {
    private static boolean hasCigarette = false;
    private static boolean hasTakeOut = false;
    private static ReentrantLock room = new ReentrantLock();
    private static Condition cigaretteSet = room.newCondition();
    private static Condition takeOutSet = room.newCondition();
    public static void main(String[] args) {

        new Thread(()->{
            room.lock();
            try{
                while (!hasCigarette){
                    log.debug("没烟，没法干活等一会儿");
                    cigaretteSet.await();
                }
                log.debug("有烟了，可以干活了兄弟");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                room.unlock();
            }
        },"张三").start();

        new Thread(()->{
            room.lock();
            try{
                while (!hasTakeOut){
                    log.debug("没外卖，没法干活等一会儿");
                    takeOutSet.await();
                }
                log.debug("外卖到了，可以干活了兄弟");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                room.unlock();
            }
        },"李四").start();

        sleep(1);

        new Thread(()->{
            room.lock();
            try {
                log.debug("烟到了。。。。");
                hasCigarette = true;
                cigaretteSet.signal();
            }finally {
                room.unlock();
            }
        },"王五").start();

        new Thread(()->{
            room.lock();
            try{
                log.debug("外卖到了。。。。");
                hasTakeOut = true;
                takeOutSet.signal();
            }finally {
                room.unlock();
            }
        },"赵六").start();
    }
}
