package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest5")
public class Test5 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t1");
        log.info("t1 status:{}",t1.getState());
        t1.start();
        Thread.sleep(500);
        log.info("t1 status:{}",t1.getState());
    }
}
