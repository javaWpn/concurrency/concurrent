package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import javax.swing.text.html.Option;
import java.util.LinkedList;
import java.util.Optional;

import static cn.itcast.n2.util.Sleeper.sleep;

@Slf4j(topic = "c.Test22")
public class Test22 {
    public static void main(String[] args) {
        MessageQueue messageQueue = new MessageQueue(3);
        for (int i = 0; i < 10; i++) {
            int id = i;
            new Thread(()-> {
                messageQueue.put(new Message(""+id,"test"+id));
            },"t"+i).start();
        }

            new Thread(()-> {
                while (true){
                    sleep(1);
                    System.out.println(messageQueue.take());
                }
            },"t").start();

    }
}


@Slf4j(topic = "c.MessageQueue")
final class MessageQueue{
    private LinkedList<Message> list = new LinkedList<>();
    private int maxNum;

    public MessageQueue(int maxNum) {
        this.maxNum = maxNum;
    }

    //获取方法
    public Message take(){
        synchronized (list){
            while (list.isEmpty()){
                log.debug("已经消费完。。请补货");
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            list.notifyAll();
            log.debug("已消费。。");
            return list.removeFirst();
        }
    }
    //存入方法
    public void put(Message message) {
        synchronized (list){
            while (list.size() == maxNum){
                log.debug("已满。。请不要补货");
                try {
                    list.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            list.add(message);
            log.debug("已补货。。");
            list.notifyAll();
        }
    }
}


class Message{

    private String id;
    private String content;

    public Message(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
