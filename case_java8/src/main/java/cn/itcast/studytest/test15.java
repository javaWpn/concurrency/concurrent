package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.test15")
public class test15 {

    static final Object lock = new Object();
    public static void main(String[] args) {
        synchronized (lock){
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
