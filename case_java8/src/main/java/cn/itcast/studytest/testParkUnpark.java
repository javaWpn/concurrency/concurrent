package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.LockSupport;

import static cn.itcast.n2.util.Sleeper.sleep;

@Slf4j(topic = "c.testParkUnpark")
public class testParkUnpark {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            log.debug("begin....");
            sleep(1);
            log.debug("park....");
            LockSupport.park();
            log.debug("recover....");
        }, "t1");

        t1.start();
        sleep(2);
        log.debug("unpark....");
        LockSupport.unpark(t1);

    }
}
