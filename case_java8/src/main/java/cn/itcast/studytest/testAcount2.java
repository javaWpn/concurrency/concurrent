package cn.itcast.studytest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class testAcount2 {

    public static void main(String[] args) {
        handelTransfer();
    }

    public static void handelTransfer(){
//        Acount2 acount2 = new Acount2(10000);
        Acount2Cas acount2 = new Acount2Cas(10000);
        List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < 1000; i++) {
            threads.add(new Thread(()->{
                acount2.thansfer(10);
            }));
        }
        long start = System.nanoTime();
        threads.forEach(Thread::start);
        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long end = System.nanoTime();
        System.out.println(acount2.getBalance()+":"+ (end-start)/1000_000 + " ms");
    }
}

class Acount2{
    private int balance;

    public Acount2(int balance) {
        this.balance = balance;
    }

    public synchronized int getBalance() {
        return balance;
    }

    public synchronized void thansfer(Integer balance){
        this.balance -= balance;
    }
}

class Acount2Cas{
    private AtomicInteger balance;

    public Acount2Cas(int balance) {
        this.balance = new AtomicInteger(balance);
    }

    public synchronized int getBalance() {
        return balance.get();
    }

    public synchronized void thansfer(Integer balance){
        while (true){
            int prev = this.balance.get();
            int next = prev - balance;
            if (this.balance.compareAndSet(prev,next)){
                break;
            }
        }
    }
}