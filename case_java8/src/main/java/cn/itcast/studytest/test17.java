package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.test16")
public class test17 {

    static final Object lock = new Object();
    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock){
                log.debug("获得锁；；；");
//                Sleeper.sleep(10); //sleep是Thead方法里的，他不会主动释放锁
                try {
                    lock.wait();//wait是Object的方法，他会主动释放锁
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t1").start();
        Sleeper.sleep(1);
        synchronized (lock){
            log.debug("获得锁；；；");
        }
    }
}
