package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.Test13")
public class Test13 {
    public static void main(String[] args) throws InterruptedException {
        Room room = new Room();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                room.increment();
            }
        }, "t1");
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                room.decrement();
            }
        }, "t2");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        log.debug("count = {}",room.getCount());
    }
}

class Room{
    private int count = 0;

    public void decrement( ) {
        synchronized (this){
            count--;
        }
    }

    public void increment( ) {
        synchronized (this){
            count++;
        }
    }

    public int getCount() {
        synchronized (this){
            return count;
        }
    }
}