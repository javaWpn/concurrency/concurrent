package cn.itcast.studytest;

public class test27 {
    public static void main(String[] args) {
        WaitNotify wn = new WaitNotify(1, 5);
        new Thread(()->{
            wn.print("a",1,2);
        },"t1").start();

        new Thread(()->{
            wn.print("b",2,3);
        },"t2").start();

        new Thread(()->{
            wn.print("c",3,1);
        },"t3").start();
    }

}

class WaitNotify{
    private final static Object lock = new Object();
    private int flag;
    private int loopNum;

    public WaitNotify(int flag, int loopNum) {
        this.flag = flag;
        this.loopNum = loopNum;
    }


    public void print(String str, int waitFlag, int nextFlag){
        synchronized (lock){
            for (int i = 0; i < loopNum; i++) {
                while (flag != waitFlag){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(str);
                flag = nextFlag;
                lock.notifyAll();
            }
        }
    }
}