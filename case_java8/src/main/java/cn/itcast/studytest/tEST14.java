package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import static cn.itcast.n2.util.Sleeper.sleep;

@Slf4j(topic = "c.Test14")
public class tEST14 {
    public static void main(String[] args) throws InterruptedException {
        RoomTest room = new RoomTest();
        new Thread(() -> {
            log.debug("begin....{}","a");
            room.a();
        }, "t1").start();
        new Thread(() -> {
            log.debug("begin....{}","b");
            room.b();
        }, "t2").start();
    }
}

@Slf4j(topic = "c.RoomTest")
class RoomTest{
    public synchronized void a(){
        sleep(1);
        log.debug("1");
    }
    public synchronized void b(){
        log.debug("2");
    }
}