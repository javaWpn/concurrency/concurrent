package cn.itcast.studytest;

public class testMoniterThread {
    public static void main(String[] args) {
        Moniter moniter = new Moniter();
        moniter.start();
        moniter.start();
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        moniter.stop();
    }
}

class Moniter {
    private Thread moniter;
    private volatile boolean stop;
    private boolean starting;

    public void  start(){
        //犹豫模式
        synchronized (this){
            if (starting){
                return;
            }
            starting = true;
        }
        moniter = new Thread(()->{
            while (true){
                if (stop){
                    System.out.println("料理后事。。。。。");
                    break;
                }
                try {
                    Thread.sleep(1000);
                    System.out.println("执行监控记录。。。。。。");
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                    stop = true;
                }
            }
        });
        moniter.start();
    }

    public void stop(){
        stop = true;
        moniter.interrupt();
    }
}
