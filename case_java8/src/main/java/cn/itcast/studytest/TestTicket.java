package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;
@Slf4j(topic = "c.TestThreadSafe")
public class TestTicket {
    public static void main(String[] args) throws InterruptedException {
        //模拟一共有1000张票
        TicketWindow ticketWindow = new TicketWindow(1000);
        //用来记录买的票数的集合
        Vector<Integer> amountList = new Vector<>();
        //保证记录所有的线程集合
        List<Thread> threadList = new ArrayList<Thread>();
        //模拟有4000人买票
        for (int i = 0; i < 4000; i++) {
            Thread thread = new Thread(() -> {
                //模拟各种情况导致的时间不一致，
                try {
                    Thread.sleep(randomInt());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int amount = ticketWindow.sell(randomInt());
                amountList.add(amount);
            }, "" + i);
            threadList.add(thread);
            thread.start();
        }
        //保证所有的线程都执行完
        for (Thread thread : threadList) {
            thread.join();
        }

        //打印余票
        log.debug("余票:{}",ticketWindow.getCount());
        //打印卖出去的票
        log.debug("卖出去的票:{}",amountList.stream().mapToInt(i->i).sum());
    }

    public static int randomInt(){
        Random random = new Random();
        return random.nextInt(5) + 1;
    }

}

class TicketWindow{

    private int count;

    public TicketWindow(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public synchronized int sell(int amount){
        if (this.count >= amount){
            this.count -= amount;
            return amount;
        }else {
            return 0;
        }
    }
}