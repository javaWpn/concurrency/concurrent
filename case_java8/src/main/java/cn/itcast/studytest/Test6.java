package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.studyTest6")
public class Test6 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            log.debug("enter sleep......");
            try {
                Thread.sleep(2000);
                log.debug("逻辑处理。。。。。。");
            } catch (InterruptedException e) {
                log.debug("wake up......");
                log.debug("t1 status:{}",Thread.currentThread().getState());
                e.printStackTrace();
                log.debug("t1 status:{}",Thread.currentThread().getState());
            }
        }, "t1");
        log.debug("t1 status:{}",t1.getState());
        t1.start();
        log.debug("t1 status:{}",t1.getState());
        Thread.sleep(500);
        log.debug("t1 status:{}",t1.getState());
        t1.interrupt();
        log.debug("t1 status:{}",t1.getState());

    }
}
