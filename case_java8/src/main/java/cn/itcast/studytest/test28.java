package cn.itcast.studytest;

import java.util.concurrent.locks.LockSupport;

public class test28 {
    static Thread t1;
    static Thread t2;
    static Thread t3;
    public static void main(String[] args) {
        WaitNotifyUnpark wn = new WaitNotifyUnpark(1, 5);
        t1 = new Thread(() -> {
            wn.print("a", t2);
        }, "t1");

        Thread t2 = new Thread(() -> {
            wn.print("b", t3);
        }, "t2");

        Thread t3 = new Thread(() -> {
            wn.print("c", t1);
        }, "t3");
    }

}

class WaitNotifyUnpark{
    private int flag;
    private int loopNum;

    public WaitNotifyUnpark(int flag, int loopNum) {
        this.flag = flag;
        this.loopNum = loopNum;
    }


    public void print(String str, Thread next){
        for (int i = 0; i < loopNum; i++) {
            LockSupport.park();
            System.out.print(str);
            LockSupport.unpark(next);
        }
    }

}