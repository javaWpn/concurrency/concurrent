package cn.itcast.studytest;

public class SingletonTest {
    private SingletonTest(){}
    private static class LazyHolder{
        final static SingletonTest singleton = new SingletonTest();
    }
    public static SingletonTest getInstance(){
        return LazyHolder.singleton;
    }

    public static void main(String[] args) {
        SingletonTest s1 = SingletonTest.getInstance();
        SingletonTest s2 = SingletonTest.getInstance();
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1==s2);
    }
}
