package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j(topic = "c.studyTest9")
public class Test9 {
    static int r = 0;
    public static void main(String[] args) throws InterruptedException {
        log.debug("开始。。。。。");
        Thread t1 = new Thread(() -> {
            log.debug("开始。。。。。");
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("结束。。。。。");
            r = 10;
        }, "t1");
        t1.start();
        t1.join();//等待线程运行结束
        log.debug("r:{}",r);
        log.debug("结束。。。。。");
    }
}
