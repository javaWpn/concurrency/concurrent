package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.OBJ_ADAPTER;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

@Slf4j(topic = "c.test20")
public class Test21 {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new People().start();
        }
        Sleeper.sleep(1);
        for (Integer id : MailBoxs.getId()) {
            new PostMan(id,"信"+id).start();
        }
    }
}
@Slf4j(topic = "c.People")
class People extends Thread{
    @Override
    public void run() {
        GuardedTest2 guardedTest2= MailBoxs.setMail();
        Object mail = guardedTest2.get(5000L);
        log.debug("收到信 id:{}, 内容:{}", guardedTest2.getId(), mail);
    }
}
@Slf4j(topic = "c.PostMan")
class PostMan extends Thread{
    private int id;
    private String mail;

    public PostMan(int id, String mail) {
        this.id = id;
        this.mail = mail;
    }

    @Override
    public void run() {
        GuardedTest2 guardedObject = MailBoxs.getMail(id);
        log.debug("送信 id:{}, 内容:{}", id, mail);
        guardedObject.complete(mail);
    }
}
class MailBoxs {
    private static Map<Integer, GuardedTest2> mailMap = new Hashtable<>();
    private static int id = 0;

    //自增id
    private static synchronized int generateId() {
        return id++;
    }

    //取件
    public static GuardedTest2 getMail(int id) {
        return mailMap.remove(id);
    }

    //放件
    public static GuardedTest2 setMail() {
        GuardedTest2 go = new GuardedTest2(generateId());
        mailMap.put(go.getId(), go);
        return go;
    }

    //获取件id
    public static Set<Integer> getId(){
        return mailMap.keySet();
    }
}
class GuardedTest2{
    private  int id;
    private  Object response;

    public GuardedTest2(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Object get(Long timeout){
        synchronized (this){
            long begin = System.currentTimeMillis();
            long passdTime = 0;
            long waitTime = timeout - passdTime;
            while (null ==this.response){
                if (waitTime <= 0){
                    break;
                }
                try {
                    this.wait(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passdTime = System.currentTimeMillis() - begin;
            }
            return  response;
        }
    }

    public void complete(Object response){
        synchronized (this){
            this.response = response;
            this.notifyAll();
        }
    }
}