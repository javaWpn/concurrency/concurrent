package cn.itcast.studytest;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "c.test26")
public class test26 {
    final static Object lock = new Object();
    static boolean hasRun2 = false;
    static ReentrantLock Lock = new ReentrantLock();
    static Condition waitSet = Lock.newCondition();
    public static void main(String[] args) {

        new Thread(()->{
            synchronized (lock){
                while (!hasRun2){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.debug("1");
            }
        },"t1").start();

        new Thread(()->{
            synchronized (lock){
                log.debug("2");
                hasRun2 = true;
                lock.notify();
            }
        },"t2").start();

        Thread t3 = new Thread(() -> {
            LockSupport.park();
            log.debug("3");
        }, "t3");

        Thread t4 = new Thread(() -> {
            log.debug("4");
            LockSupport.unpark(t3);
        }, "t4");

        t3.start();
        t4.start();


        Thread t5 = new Thread(() -> {
            Lock.lock();
            try {
                waitSet.await();
                log.debug("5");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                Lock.unlock();
            }
        }, "t5");

        Thread t6 = new Thread(() -> {
            Lock.lock();
            try {
                log.debug("6");
                waitSet.signal();
            } finally {
                Lock.unlock();
            }
        }, "t6");

        t5.start();
        t6.start();
    }
}
