package cn.itcast.studytest;

import cn.itcast.n2.util.Sleeper;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "c.test18")
public class test18 {
    static final Object lock = new Object();
    static boolean hasCigaratte = false;//是否有烟
    static boolean hasTakeout = false;//是否有外卖
    public static void main(String[] args) {
        new Thread(()->{
            synchronized (lock){
                log.debug("拿到锁了");
                //用while代替if解决虚假唤醒的问题
                while(!hasCigaratte){
                    log.debug("没烟，等烟到了再说。。。。");
//                    Sleeper.sleep(2);//其他人无法获得锁资源
                    try {
                        lock.wait();//其他人可以获得锁资源
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (hasCigaratte){
                    log.debug("开始干活");
                }else{
                    log.debug("没干成活。。。。");
                }
            }
        },"小南").start();

        new Thread(()->{
            synchronized (lock){
                log.debug("拿到锁了");
                while(!hasTakeout){
                    log.debug("没外卖，等外卖到了再说。。。。");
//                    Sleeper.sleep(2);//其他人无法获得锁资源
                    try {
                        lock.wait();//其他人可以获得锁资源
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (hasTakeout){
                    log.debug("开始干活");
                }else{
                    log.debug("没干成活。。。。");
                }
            }
        },"小北").start();

        int loopNumer = 5;
        for (int i = 0; i < loopNumer; i++) {
            new Thread(()->{
                synchronized (lock){
                    log.debug("开始干活");
                }
            },"其他人"+i).start();
        }
        Sleeper.sleep(1);
        new Thread(()->{
            log.debug("外卖送到了。。。。。");
            synchronized (lock){
                hasTakeout = true;
//                lock.notify();//错误的叫醒其他线程，所以此处得用notifyAll能解决小北的送外卖问题,但是也解决不了小南的问题
                lock.notifyAll();
            }
        },"送外卖的").start();
    }
}
