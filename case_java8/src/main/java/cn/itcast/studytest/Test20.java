package cn.itcast.studytest;

import cn.itcast.pattern.Downloader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

@Slf4j(topic = "c.test20")
public class Test20 {
    public static void main(String[] args) {
        GuardedTest guardedTest = new GuardedTest();
        new Thread(()->{
            List<String>  list = (List<String> )guardedTest.get(4000);
            log.debug("结果大小：{}",list.size());
        },"t1").start();

        new Thread(()->{
            try {
                List<String> result = Downloader.download();
                guardedTest.complete(result);
            } catch (IOException e) {
                e.printStackTrace();
            }

        },"t2").start();
    }
}


class GuardedTest{
    private int id;
    private Object response;

    public int getId() {
        return id;
    }

    public Object get(long timeout){
        synchronized (this){
            long begin = System.currentTimeMillis();
            long passTime = 0;
            long waitTime = timeout - passTime;
            while (null == this.response){
                if (waitTime <= 0){
                    break;
                }
                try {
                    this.wait(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passTime = System.currentTimeMillis() - begin;
            }
            return this.response;
        }
    }

    public void complete(Object response){
        synchronized (this){
            this.response = response;
            this.notifyAll();
        }
    }
}